const perceptron = () => {
    const weights = []
    const bias =  1;
    var learningrate = 0.1;
    let data = [];
    
    const retrain = () => {
        const length = data.length
        let success = true
        for (var i = 0; i < length; i++) {
            const training = data.shift()
            success = train(training.input, training.target) && success
        }
        return success
    }

    const train = (inputs, expected) => {
        while (weights.length < inputs.length) {
            weights.push(1)
        }
        
        if (weights.length == inputs.length) {
            weights.push(bias)
        }

        var result = predict(inputs)
        data.push({ input: inputs, target: expected, prev: result })

        if (result == expected) {
            return true
        } else {
            for (var i = 0; i < weights.length; i++) {
                var input = (i == inputs.length)
                    ? bias
                    : inputs[i]
                adjust(result, expected, input, i)
            }
            return false
        }
    }

    const adjust = (result, expected, input, index) => {
        const delta = (expected - result) * learningrate * input;
        weights[index] += delta;
    }

    const activation = (x) => { return x > 0 ? 1 : -1 };

    const predict = (inputs) => {
        var result = 0
        for (var i = 0; i < inputs.length; i++) {
            result += inputs[i] * weights[i]
        }
        result += bias * weights[weights.length - 1];
        return activation(result);
    }

    return {
        predict, 
        train,
        retrain,
    };
}

const execution = perceptron();

const inputs = [
    { x1: 1, x2: 1, expected: 1 },
    { x1: 9.4, x2: 6.4, expected: -1 },
    { x1: 2.5, x2: 2.1, expected: 1 },
    { x1: 8, x2: 7.7, expected: -1 },
    { x1: 0.5, x2: 2.2, expected: 1 },
    { x1: 7.9, x2: 8.4, expected: -1 },
    { x1: 7, x2: 7, expected: -1 },
    { x1: 2.8, x2: 0.8, expected: 1 },
    { x1: 1.2, x2: 3.0, expected: 1 },
    { x1: 7.8, x2: 6.1, expected: -1 },
]

inputs.forEach(input => {
    execution.train([input.x1, input.x2], input.expected)
})

while (!execution.retrain()) { }

inputs.forEach(input => {
    console.log(`x1: ${input.x1} | x2: ${input.x2} | class: ${execution.predict([input.x1, input.x2])}`);
})

if(process.argv.length == 4){
    const x1 = process.argv[2];
    const x2 = process.argv[3];
    console.log("\n\n\nPredicting...")
    console.log(`x1: ${x1} | x2: ${x2} | result:  ${execution.predict([x1, x2])}`);
}