var _ = require("underscore");
const properties = [
    "HC",
    "GARANTIA",
    "DIVIDA",
    "RENDA",
];

// RESULT  
// {   entry: "",
//     label: "",
//     nodes: [{
//         entry: "",
//         label: "",
//         nodes: [],
//     }]
// }

const data = [{ "RISCO": "alto", "HC": "ruim", "DIVIDA": "alta", "GARANTIA": "nenhuma", "RENDA": "$0 a $15k" }, { "RISCO": "alto", "HC": "desconhecido", "DIVIDA": "alta", "GARANTIA": "nenhuma", "RENDA": "$15 a $35k" }, { "RISCO": "moderado", "HC": "desconhecido", "DIVIDA": "baixa", "GARANTIA": "nenhuma", "RENDA": "$15 a $35k" }, { "RISCO": "alto", "HC": "desconhecido", "DIVIDA": "baixa", "GARANTIA": "nenhuma", "RENDA": "$0 a $15k" }, { "RISCO": "baixo", "HC": "desconhecido", "DIVIDA": "baixa", "GARANTIA": "nenhuma", "RENDA": "acima de $35k" }, { "RISCO": "baixo", "HC": "desconhecido", "DIVIDA": "baixa", "GARANTIA": "adequada", "RENDA": "acima de $35k" }, { "RISCO": "alto", "HC": "ruim", "DIVIDA": "baixa", "GARANTIA": "nenhuma", "RENDA": "$0 a $15k" }, { "RISCO": "moderado", "HC": "ruim", "DIVIDA": "baixa", "GARANTIA": "adequada", "RENDA": "acima de $35k" }, { "RISCO": "baixo", "HC": "boa", "DIVIDA": "baixa", "GARANTIA": "nenhuma", "RENDA": "acima de $35k" }, { "RISCO": "baixo", "HC": "boa", "DIVIDA": "alta", "GARANTIA": "adequada", "RENDA": "acima de $35k" }, { "RISCO": "alto", "HC": "boa", "DIVIDA": "alta", "GARANTIA": "nenhuma", "RENDA": "$0 a $15k" }, { "RISCO": "moderado", "HC": "boa", "DIVIDA": "alta", "GARANTIA": "nenhuma", "RENDA": "$15 a $35k" }, { "RISCO": "baixo", "HC": "boa", "DIVIDA": "alta", "GARANTIA": "nenhuma", "RENDA": "acima de $35k" }, { "RISCO": "alto", "HC": "ruim", "DIVIDA": "alta", "GARANTIA": "nenhuma", "RENDA": "$15 a $35k" }];


function id3(sample = data, clazz = "RISCO", entries = properties) {
    if ([...new Set(sample.map(a => a[clazz]))].length == 1) {
        return { entry: clazz, label: sample[0][clazz], };
    }
    if (entries.length == 0) {
        return sample.map(a => ({ entry: clazz, label: a[clazz] }));
    }
    const entry = selectBest([...sample], clazz, entries);
    const newEntries = entries.filter(a => { return a != entry; });
    return groupBy(sample, entry)
        .map(keyValue => ({
            entry,
            label: keyValue[0],
            nodes: id3(keyValue[1], clazz, newEntries)
        }));
}

console.log(JSON.stringify(id3(), null, 2));

// selection func
function selectBest(s, target, features, shoudCalculate = false) {

    if (!shoudCalculate) {
        return features[0];
    }

    var _s = _(s);

    function entropy(vals) {
        var uniqueVals = _.unique(vals);
        var probs = uniqueVals.map(function (x) { return prob(x, vals) });
        var logVals = probs.map(function (p) { return -p * log2(p) });
        return logVals.reduce(function (a, b) { return a + b }, 0);
    }

    function gain(_s, target, feature) {
        var attrVals = _.unique(_s.pluck(feature));
        var setEntropy = entropy(_s.pluck(target));
        var setSize = _s.size();
        var entropies = attrVals.map(function (n) {
            var subset = _s.filter(function (x) { return x[feature] === n });
            return (subset.length / setSize) * entropy(_.pluck(subset, target));
        });
        var sumOfEntropies = entropies.reduce(function (a, b) { return a + b }, 0);
        return setEntropy - sumOfEntropies;
    }

    function maxGain(_s, target, features) {
        return _.max(features, function (e) { return gain(_s, target, e) });
    }

    function prob(val, vals) {
        var instances = _.filter(vals, function (x) { return x === val }).length;
        var total = vals.length;
        return instances / total;
    }

    function log2(n) {
        return Math.log(n) / Math.log(2);
    }

    return maxGain(_s, target, features);
}

// aux func
function groupBy(xs, key) {
    return Object.entries(xs.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {}));
};